from django.db import models
from projects.models import Project
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL
CHOICES_YES_NO = ((True, ("yes")), (False, ("no")))


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(
        choices=CHOICES_YES_NO,
        default=False,
    )
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        USER_MODEL,
        models.SET_NULL,
        related_name="tasks",
        null=True,
    )

    def __str__(self):
        return self.name


# Create your models here.
